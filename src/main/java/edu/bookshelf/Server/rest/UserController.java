package edu.bookshelf.Server.rest;

import edu.bookshelf.Server.rest.repository.User;
import edu.bookshelf.Server.rest.repository.UserRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RequestMapping("api/v1/lib")
@RestController
public class UserController {

    //private final BookRepository bookRepository;
    private final UserRepository userRepository;

    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
    // TODO compact user view (without books, just book count)
    @GetMapping("{user}")
    public User getUserInfo(@PathVariable("user") Long id) {
        return userRepository.findById(id).get();
    }

    @GetMapping("users")
    public List<User> getUserList() {
        return userRepository.findAll();
    }

    @PostMapping("add")
    public void addNewUser(@RequestBody User user) {
        userRepository.save(user);
    }

    @DeleteMapping("{user}") //type?
    public void deleteUser(@PathVariable("user") Long id) {
        userRepository.deleteById(id);
    }


}
