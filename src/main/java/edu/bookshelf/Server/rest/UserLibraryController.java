package edu.bookshelf.Server.rest;

import edu.bookshelf.Server.rest.repository.Book;
import edu.bookshelf.Server.rest.repository.BookRepository;
import edu.bookshelf.Server.rest.repository.User;
import edu.bookshelf.Server.rest.repository.UserRepository;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.Set;

@RequestMapping("api/v1/lib")
@RestController
public class UserLibraryController {

    private final UserRepository userRepository;
    private final BookRepository bookRepository;

    public UserLibraryController(UserRepository userRepository, BookRepository bookRepository) {
        this.bookRepository = bookRepository;
        this.userRepository = userRepository;
    }


    @PostMapping("{user}/add") // user/add?name=something&isbn=XXX
    public void addBookEntry(@PathVariable("user") Long id, // FIXME: anyone can add books to anybody else
                             @RequestBody Book book) {    // Either implement security, or have context-dependent controller
        if( userRepository.findById(id).isEmpty() ) return;
        User usr = userRepository.findById(id).get();
        usr.appendUserLibrary(book);
        userRepository.save(usr);
    }
    //@PostMapping("{user}")

    @GetMapping(path = "{user}/books")
    public Set<Book> getAllBooksForUser(@PathVariable("user") Long id) {
        return userRepository.getReferenceById(id).getUserLibrary();
    }

    @DeleteMapping(path = "{user}/{book}")
    public void deleteLibraryEntry(@PathVariable("user") Long id, @PathVariable("book") Long isbn) {
        if( bookRepository.findById(isbn).isEmpty() || userRepository.findById(id).isEmpty()) return;
        Book bk = bookRepository.findById(isbn).get();
        User usr = userRepository.findById(id).get();
        usr.removeUserLibraryEntry(bk);
        userRepository.save(usr);
    }

    @GetMapping(path = "testhtml", produces = MediaType.TEXT_HTML_VALUE)
    public String test2() {
        return "<!DOCTYPE html>\n" +
                "<html>\n" +
                "    <head>\n" +
                "        <title>Example</title>\n" +
                "    </head>\n" +
                "    <body>\n" +
                "        <p>This is an example of a simple HTML page with one paragraph.</p>\n" +
                "    </body>\n" +
                "</html>\n";
    }

    @PostMapping("/test")
    public void testApi() {
        Book book = null;
        Book book0;
        if (!userRepository.existsById(0L)) {
            userRepository.save(new User("martin", "1234"));
            book = new Book("Thomas Sawyer", 11234L);
            book0 = new Book("Conan Doyle", 12556L);
            bookRepository.save(book);
            bookRepository.save(book0);
        }

        User martin = userRepository.findByUsername("martin");
        Set<Book> mset = new HashSet<Book>();
        mset.add(book);
        martin.setUserLibrary(mset);
        // test cascading
        userRepository.save(martin); // important: commit changes!
    }
}