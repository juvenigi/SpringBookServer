package edu.bookshelf.Server.rest.repository;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity @Table(name = "Books")
public class Book implements Serializable {
    @Id @Column(unique = true)
    private Long isbn; // TODO validate isbn (length in decimal form, remove hyphens if a string is inputted)
    @Column(name = "name")
    private String bookName;

    Book(){};
    public Book(String name,
                Long isbn) {
        this.isbn = isbn;
        this.bookName = name;
    }

    public Long getIsbn() {
        return isbn;
    }

    public void setIsbn(Long isbn) {
        this.isbn = isbn;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }
}
