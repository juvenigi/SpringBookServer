package edu.bookshelf.Server.rest.repository;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
    User findByPassword(String password); //FIXME missing editor config files?
    //List<User> findUsersByBook(Book book);
}
