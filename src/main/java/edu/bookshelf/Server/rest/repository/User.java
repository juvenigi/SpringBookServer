package edu.bookshelf.Server.rest.repository;


import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity @Table(name="users")
public class User implements Serializable { // NOTE: should it implement Serializable?

    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private Long userID; // I have a strong disdain towards the `Id` style of writing

    private String username;

    private String password;

    @ManyToMany(fetch = FetchType.EAGER,
            cascade = {
                CascadeType.PERSIST,
                CascadeType.MERGE,
                //CascadeType.REMOVE,
                CascadeType.DETACH,
                CascadeType.REFRESH,
            })
    private Set<Book> userLibrary;

    User () {} // reflection
    public User(String username, String password) {
        //this.userID = userID;
        this.username = username;
        this.password = password;
    }

    public Set<Book> getUserLibrary() {
        return userLibrary;
    }

    public void appendUserLibrary(Book book) {
        this.userLibrary.add(book);
    }

    public void setUserLibrary(Set<Book> book) {
        this.userLibrary = book;
    }
    public void removeUserLibraryEntry(Book book) {
        this.userLibrary.remove(book);
    }

    public void clearUserLibrary() {
        this.userLibrary = new HashSet<Book>();
    }
    public boolean hasBook(Book book) {
        return userLibrary.contains(book);
    }
    public Long getUserID() {
        return this.userID;
    }

    public void setUserID(Long id) {
        this.userID = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername() {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}