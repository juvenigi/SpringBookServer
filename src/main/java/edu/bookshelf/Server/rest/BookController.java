package edu.bookshelf.Server.rest;

import edu.bookshelf.Server.home.UserService;
import edu.bookshelf.Server.rest.repository.Book;
import edu.bookshelf.Server.rest.repository.BookRepository;
import edu.bookshelf.Server.rest.repository.UserRepository;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("api/v1/lib")
@RestController
public class BookController {

    private final BookRepository bookRepository;
    //private final UserRepository userRepository;
    private final UserService userService;

    public BookController(BookRepository bookRepository, UserRepository userRepository, UserService userService) {
        this.bookRepository = bookRepository;
        //this.userRepository = userRepository;
        this.userService = userService;
    }

    @GetMapping("books")
    public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    @GetMapping("books/{book}")
    public Book getBookInfo(@PathVariable("book") Long isbn) {
        return bookRepository.findById(isbn).get();
    }

    @DeleteMapping("books/{book}")
    public void deleteBook(@PathVariable("book") Long isbn) {
        // retrieve book
        Book book = bookRepository.findById(isbn).get();
        // first, find all references and delete them
        userService.deleteBooksFromUser(isbn, book);
    }


    //@PostMapping("books/clear")

    @RequestMapping(value = "books/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void addBook(@RequestBody Book book) {
        bookRepository.save(book);
    }
//    @PostMapping(value = "books/add")
//    public void addBook(@ModelAttribute @Validated Book book){
//        bookRepository.save(book);
//    }
}
