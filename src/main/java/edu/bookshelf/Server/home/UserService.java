package edu.bookshelf.Server.home;

import edu.bookshelf.Server.rest.repository.Book;
import edu.bookshelf.Server.rest.repository.BookRepository;
import edu.bookshelf.Server.rest.repository.User;
import edu.bookshelf.Server.rest.repository.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private UserRepository userRepository;
    private BookRepository bookRepository;

    public UserRepository getUserRepository() {
        return userRepository;
    }

    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public BookRepository getBookRepository() {
        return bookRepository;
    }

    public void setBookRepository(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public void deleteBooksFromUser(Long isbn, Book book) {
        userRepository.findAll().stream()
                .filter(user -> user.hasBook(book))
                .forEach(user -> {
                    user.removeUserLibraryEntry(book);
                    userRepository.save(user);
                });
        bookRepository.deleteById(isbn);
    }

    public void addBookToUser(Long userID, Book book) {
        User user = userRepository.findById(userID).get();
        user.appendUserLibrary(book);
    }

}