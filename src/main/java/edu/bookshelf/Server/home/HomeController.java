package edu.bookshelf.Server.home;

import edu.bookshelf.Server.rest.repository.Book;
import edu.bookshelf.Server.rest.repository.BookRepository;
import edu.bookshelf.Server.rest.repository.UserRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class HomeController {

    private final UserRepository userRepository;
private final UserService userService;
    private final BookRepository bookRepository;
    public HomeController(UserRepository userRepository, UserService userService, BookRepository bookRepository){
        this.userRepository = userRepository;
        this.userService = userService;
        this.bookRepository = bookRepository;
    }

    @GetMapping("userbooks")
    public String greet(Model model, @PathVariable(value = "user") Long id) {
        if (userRepository.findById(id).isEmpty()) return "error";
        model.addAttribute("user", userRepository.findById(id).get());
        model.addAttribute("books", bookRepository.findAll());
        return "userbooks";
    }

    @GetMapping("library")
    public String library(Model model) {

        return "library";
    }

    @DeleteMapping("library")
    public String deleteLibraryEntry(Model model) {
        return "library";
    }

    @GetMapping("users")
    public String users(Model model) {

        return "users";
    }

    @PostMapping("addBook") // adds a new book to user
    public String addBook(@RequestParam(value = "user") Long id, @RequestBody Book book) {
        if (userRepository.findById(id).isEmpty()) return "error";
        userService.addBookToUser(id, book);
        return "userbooks";
    }

    @DeleteMapping(value = "/remove")
    public ResponseEntity<?> handleRemoveBook() {

        return ResponseEntity.ok().body(null);
    }

    @DeleteMapping(value = "/removeUser")
    public ResponseEntity<?> handleRemoveUser() {

        return ResponseEntity.ok().body(null);
    }

    @PostMapping(value = "login")
    public String postLogin() {
        return "userbooks";
    }

    @GetMapping(value = {"/", "welcome", "login", "error"})
    public String welcome(Model model) {
        return "login";
    }
}
