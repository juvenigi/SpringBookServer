//package edu.bookshelf.Server;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.security.web.SecurityFilterChain;
//
//
//// NOTE: the filter chain is not optimal
//// https://spring.io/blog/2022/02/21/spring-security-without-the-websecurityconfigureradapter
//@Configuration
//@EnableWebSecurity
//@EnableGlobalMethodSecurity(securedEnabled = true, proxyTargetClass = true)
//public class SecurityConfiguration {
//
//    //@Autowired
//    private UserDetailsService userDetailsService;
//
//    @Bean
//    public PasswordEncoder passwordEncoder() {
//        return new BCryptPasswordEncoder();
//    }
//
//    @Bean
//    public SecurityFilterChain filterChain(HttpSecurity http)
//    throws Exception {
//        /*
//        http
//            .csrf().disable()
//            .authorizeRequests()
//            .antMatchers("/registration")
//            .permitAll().anyRequest()
//            .authenticated()
//            .and()
//            .httpBasic();
//         */
//        http.authorizeRequests()
//            .antMatchers("/resources/**","/registration").permitAll()
//            .anyRequest().authenticated()
//            .and()
//            .formLogin()
//            .loginPage("/login")
//            .defaultSuccessUrl("/welcome")
//            .failureUrl("/login?error")
//            .permitAll()
//            .and()
//            .logout()
//            .permitAll();
//
//        return http.build();
//    }
//
//    @Autowired
//    public void ConfigureGlobal(AuthenticationManagerBuilder auth) throws Exception {
//        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
//    }
//}
